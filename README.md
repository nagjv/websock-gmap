# websock-gmap
nodejs & reactjs google map example

### Start servers
```sh
Start local-server: node app.js
Start React UI: export REACT_APP_MAP_KEY=<GOOGLE_MAPS_KEY> && npm start
```
