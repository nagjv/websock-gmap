const WebSocket = require('ws');
const {PubSub} = require('@google-cloud/pubsub');

const projectId = process.env.PROJECT;
const subName = process.env.TOPIC_SUB;
console.log("PROJECT::", projectId)
console.log("TOPIC_SUB::", subName)

const pubsub = new PubSub(projectId)
const wss = new WebSocket.Server({ port: 8080 });

const relayPropertyData = (event) => {
  console.log("relayPropertyData:: ", event)

  wss.clients.forEach(function each(client) {
    console.log(client.readyState, WebSocket.OPEN);
    if (client.readyState === WebSocket.OPEN) {
      console.log("sending data")
      client.send(JSON.stringify(event));
    }
  });

}

wss.on('connection', function connection(ws) {
    console.log("socket connection established");

    const subscription = pubsub.subscription(subName);

    const messageHandler = message => {
        data = message.data
        data = JSON.parse(data.toString())
        console.log("Message:: ", data);
        relayPropertyData(data)
        message.ack();
    };
   
    subscription.on(`message`, messageHandler);
});
