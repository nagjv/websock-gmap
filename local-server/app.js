const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 8080 });

const sockSendEvent = (event) => {

  wss.clients.forEach(function each(client) {
    console.log(client.readyState, WebSocket.OPEN);
    if (client.readyState === WebSocket.OPEN) {
      console.log("event to client:: ", event)
      client.send(JSON.stringify(event));
    }
  });

}

const prepEvents = () => {

  //sample data
  const data = {
    properties: [
      {
        "lat": 30.299569,
        "lng": -97.778366,
        "price": "$853,000"
      },
      {
        "lat": 25.784157,
        "lng": -80.135639,
        "price": "$2,785,000"
      },
      {
        "lat": 34.096461,
        "lng": -84.271175,
        "price": "$4,500,000"
      },
      {
        "lat": 42.036132,
        "lng": -87.886506,
        "price": "$624,000"
      },
      {
        "lat": 28.043811,
        "lng": -99.352746,
        "price": "$240,000"
      },
      {
        "lat": 38.690060,
        "lng": -104.692690,
        "price": "$395,000"
      },
      {
        "lat": 46.870308,
        "lng": -68.008829,
        "price": "$195,000"
      },
      {
        "lat": 38.903927,
        "lng": -77.011545,
        "price": "$770,000"
      },
      {
        "lat": 31.850790,
        "lng": -102.414762,
        "price": "$170,000"
      },
      {
        "lat": 35.185328,
        "lng": -114.047891,
        "price": "$299,000"
      },
      {
        "lat": 33.771884,
        "lng": -118.173342,
        "price": "$1,270,000"
      },
      {
        "lat": 37.967235,
        "lng": -122.343953,
        "price": "$2,340,000"
      },
      {
        "lat": 48.259930,
        "lng": -101.304748,
        "price": "$215,000"
      },
    ]
  }

  for(let i = 0; i < data.properties.length; i++) {

    //push an event every 5 mins
    setTimeout(sockSendEvent, 5000*i, data.properties[i])
  }

}

wss.on('connection', function connection(ws) {
    console.log("sock connected");
    prepEvents()
});
