import React, { Component } from 'react'
import GoogleMap from 'google-map-react';

const mapStyles = {
  width: '50%',
  height: '50%',
  margin: 50,
  padding: 200,

}

const markerStyle = {
  height: '50px',
  width: '50px',
  marginTop: "-50px"
}

const imgStyle = {
  height: '40%'
}

const Marker = ({ title }) => (
  <div style={markerStyle}>
    <img style={imgStyle} alt={title} src="http://maps.google.com/mapfiles/kml/pal3/icon56.png" />
    <h3>{title}</h3>
  </div>
);

const URL = process.env.WEBSOCK_API

class Map extends Component {

  constructor(props) {
    super(props)

    this.state = {
      properties: [
        {
          "lat": 46.879244,
          "lng": -124.106179,
          "price": "$999,999"
        }
      ]
    }
  }

  ws = new WebSocket(URL)

  componentDidMount() {
    this.ws.onopen = () => {
      console.log('UI connected!')
    }

    this.ws.onmessage = evt => {
      // on receiving a property, add it to the list of properties
      console.log(evt.data)
      const property = JSON.parse(evt.data)
      this.addProperty(property)
    }

    this.ws.onclose = () => {
      console.log('Socket closed!')
      // automatically try to reconnect on connection loss
      this.setState({
        ws: new WebSocket(URL),
      })
    }
  }

  addProperty = property =>
  this.setState(state => ({ properties: [property, ...state.properties] }))

  render() {
    console.log("process.env.REACT_APP_MAP_KEY::", process.env.REACT_APP_MAP_KEY)
    return (
      <div>
        <GoogleMap
          style={mapStyles}
          bootstrapURLKeys={{ key: process.env.REACT_APP_MAP_KEY }}
          center={{ lat: 38.644336, lng: -100.535781 }}
          zoom={5}
        >
        {this.state.properties.map((property, index) =>
          <Marker key={index}
            title={property.price}
            lat={property.lat}
            lng={property.lng}
          >
          </Marker>
        )}

        </GoogleMap>
      </div>
    )
  }
}

export default Map
